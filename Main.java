import java.util.ArrayList;
import java.util.List;

public class Main {
    public static Polinom parsePolinom(String str) {
        int i;
        List<Monom> monoamePolinom=new ArrayList<>();
        String fields[]= str.split("\\+");
        for(i=0;i<fields.length;i++){
            String fields2[]=fields[i].split("X\\^");
            if(fields2[0].equals("")){
                Monom monom=new Monom(1,Integer.parseInt(fields2[1]));
                monoamePolinom.add(monom);
            }
            else {
                Monom monom=new Monom(Integer.parseInt(fields2[0]),Integer.parseInt(fields2[1]));
                monoamePolinom.add(monom);
            }
        }
        return new Polinom(monoamePolinom);
    }
    public static void main(String[] args){
        View view=new View();
        view.getPolinom1().setText("7X^4+3X^2+2X^1");
        view.getPolinom2().setText("X^2+X^1");


        view.getAdunare().addActionListener(e -> {
            String str1 = view.getPolinom1().getText();
            String str2=view.getPolinom2().getText();
            Polinom poli1=parsePolinom(str1);
            Polinom poli2=parsePolinom(str2);

            poli1.adunare(poli2);

            view.getRezultat().setText(poli1.toString());

            System.out.println(str1.toString());
            System.out.println(str2.toString());
        });

        view.getScadere().addActionListener(e -> {
            String str1 = view.getPolinom1().getText();
            String str2=view.getPolinom2().getText();
            Polinom poli1=parsePolinom(str1);
            Polinom poli2=parsePolinom(str2);

            poli1.scadere(poli2);

            view.getRezultat().setText(poli1.toString());

            System.out.println(str1.toString());
            System.out.println(str2.toString());
        });

        view.getInmultire().addActionListener(e -> {
            String str1=view.getPolinom1().getText();
            String str2=view.getPolinom2().getText();
            Polinom poli1=parsePolinom(str1);
            Polinom poli2=parsePolinom(str2);

            Polinom rez=new Polinom(poli1.inmultire(poli2));

            view.getRezultat().setText(rez.toString());


        });

        view.getDerivare().addActionListener(e ->{
            String str1=view.getPolinom1().getText();
            Polinom poli1=parsePolinom(str1);

            Polinom rez=new Polinom(poli1.derivare());

            view.getRezultat().setText(rez.toString());
        });

        view.getIntegrare().addActionListener(e ->{
            String str1=view.getPolinom1().getText();
            Polinom poli1=parsePolinom(str1);

            Polinom rez=new Polinom(poli1.integrare());

            view.getRezultat().setText(rez.toString());
        });
    }
}

