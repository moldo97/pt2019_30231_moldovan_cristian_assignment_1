import org.omg.CORBA.PRIVATE_MEMBER;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;

public class View extends JFrame {
    //Components
    private  JTextField  polinom1 = new JTextField(10);
    private JTextField polinom2 = new JTextField(10);
    private JTextField rezultat = new JTextField(20);


    private JButton adunare=new JButton("+");
    private JButton scadere=new JButton("-");
    private JButton inmultire=new JButton("*");
    private JButton impartire=new JButton("/");
    private JButton derivare=new JButton("Derivare");
    private JButton integrare=new JButton("Integrare");


    //Constructor
    View(){
        JPanel content=new JPanel();
        content.setLayout(new FlowLayout());
        content.add(new JLabel("Polinom 1"));
        content.add(polinom1);
        content.add(new JLabel("Polinom 2"));
        content.add(polinom2);
        content.add(new JLabel("Rezultat"));
        content.add(rezultat);
        content.add(adunare);
        content.add(scadere);
        content.add(inmultire);
        content.add(impartire);
        content.add(derivare);
        content.add(integrare);


        this.setContentPane(content);
        this.pack();
        this.setTitle("Polinoame");
        this.setVisible(true);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public JTextField getPolinom1() { return this.polinom1; }

    public JTextField getPolinom2() { return this.polinom2; }

    public JTextField getRezultat() { return this.rezultat; }

    public JButton getAdunare() { return this.adunare; }

    public JButton getScadere() { return this.scadere; }

    public JButton getInmultire() { return this.inmultire; }

    public JButton getDerivare() { return this.derivare; }

    public JButton getIntegrare() { return this.integrare; }
}


