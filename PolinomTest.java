import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PolinomTest {

    @Test
    public void adunare() {
        Polinom pol1=Main.parsePolinom("X^1+X^0");
        Polinom pol2=Main.parsePolinom("X^2+X^1");
        Integer m1g=2;
        Integer m1c=1;
        Integer m2g=1;
        Integer m2c=2;
        pol1.adunare(pol2);
        String s=pol1.toString();

        //Pt primul monom
        assertEquals(m1g,pol1.getPol().get(0).getGrad());
        assertEquals(m1c,pol1.getPol().get(0).getCoef());

        //Al doilea monom
        assertEquals(m2g,pol1.getPol().get(1).getGrad());
        assertEquals(m2c,pol1.getPol().get(1).getCoef());



    }

    @Test
    public void scadere() {
        Polinom pol1=Main.parsePolinom("X^1+X^0");
        Polinom pol2=Main.parsePolinom("X^2+X^1");
        Integer m1g=2;
        Integer m1c=-1;
        pol1.scadere(pol2);

        assertEquals(3,pol1.getPol().size());
        String s=pol1.toString();

        //Pt primul monom
        assertEquals(m1g,pol1.getPol().get(0).getGrad());
        assertEquals(m1c,pol1.getPol().get(0).getCoef());

    }

    @Test
    public void inmultire() {
        Polinom pol1=Main.parsePolinom("X^1+X^0");
        Polinom pol2=Main.parsePolinom("X^2+X^1");
        Integer m1g=3;
        Integer m1c=1;
        Integer m2g=2;
        Integer m2c=2;
        Polinom p=new Polinom(pol1.inmultire(pol2));

        assertEquals(3,p.getPol().size());
        String s=p.toString();

        //Pt primul monom
        assertEquals(m1g,p.getPol().get(0).getGrad());
        assertEquals(m1c,p.getPol().get(0).getCoef());

        //Al doilea monom
        assertEquals(m2g,p.getPol().get(1).getGrad());
        assertEquals(m2c,p.getPol().get(1).getCoef());
    }

    @Test
    public void derivare() {
        Polinom pol1=Main.parsePolinom("4X^3+6X^2+X^1+X^0");
        Integer m1g=2;
        Integer m1c=12;
        Integer m2g=1;
        Polinom p=new Polinom(pol1.derivare());

        assertEquals(4,p.getPol().size());
        String s=p.toString();

        //Pt primul monom
        assertEquals(m1g,p.getPol().get(0).getGrad());
        assertEquals(m1c,p.getPol().get(0).getCoef());

        //Al doilea monom
        assertEquals(m2g,p.getPol().get(1).getGrad());
        assertEquals(m1c,p.getPol().get(1).getCoef());
    }


    @Test
    public void integrare() {
        Polinom pol1=Main.parsePolinom("4X^3+6X^2+X^1+X^0");
        Integer m1g=4;
        Integer m1c=1;
        Integer m2g=3;
        Integer m2c=2;
        Polinom p=new Polinom(pol1.integrare());

        assertEquals(4,p.getPol().size());
        String s=p.toString();

        //Pt primul monom
        assertEquals(m1g,p.getPol().get(0).getGrad());
        assertEquals(m1c,p.getPol().get(0).getCoef());

        //Al doilea monom
        assertEquals(m2g,p.getPol().get(1).getGrad());
        assertEquals(m2c,p.getPol().get(1).getCoef());
    }
}