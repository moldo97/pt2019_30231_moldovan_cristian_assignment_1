import java.util.Comparator;

public class Monom {
    private Integer coef;

    public Monom(Integer coef, Integer grad) {
        this.coef = coef;
        this.grad = grad;
    }

    public Integer getCoef() {
        return coef;
    }

    public void setCoef(Integer coef) {
        this.coef = coef;
    }

    public Integer getGrad() {
        return grad;
    }

    public void setGrad(Integer grad) {
        this.grad = grad;
    }

    private Integer grad;


}


