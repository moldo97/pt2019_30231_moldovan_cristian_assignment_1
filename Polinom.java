import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Polinom {
    private List<Monom> p1;

    public Polinom(List<Monom> p1) {
        this.p1 = p1;
    }

    public List<Monom> getPol(){
        return this.p1;
    }

    public void adunare(Polinom p2){
        for(int i=0;i<p1.size();i++){
            for(int j=0;j<p2.getPol().size();j++) {
                Monom m1 = p1.get(i);
                Monom m2 = p2.getPol().get(j);
                if (m1.getGrad().equals(m2.getGrad())) {
                    m1.setCoef(m1.getCoef() + m2.getCoef());
                    p2.getPol().remove(j);
                }
            }
        }
        for(int i=0;i<p2.getPol().size();i++){
            Monom m=p2.getPol().get(i);
            p1.add(m);
        }
    }
    public void scadere(Polinom p2){
        for(int i=0;i<p1.size();i++){
            for(int j=0;j<p2.getPol().size();j++) {
                Monom m1 = p1.get(i);
                Monom m2 = p2.getPol().get(j);
                if (m1.getGrad().equals(m2.getGrad())) {
                    m1.setCoef(m1.getCoef() - m2.getCoef());
                    p2.getPol().remove(j);
                }
            }
        }
        for(int i=0;i<p2.getPol().size();i++){
            Monom m=p2.getPol().get(i);
            m.setCoef(m.getCoef()*(-1));
            p1.add(m);
        }
    }

    public List<Monom> inmultire(Polinom p2){
        List<Monom> pol=new ArrayList<>();
        for(int i=0;i<p1.size();i++){
            Monom m1=p1.get(i);
            for(int j=0;j<p2.getPol().size();j++){
                Monom m2=p2.getPol().get(j);
                Monom rez=new Monom(m1.getCoef()*m2.getCoef(),m1.getGrad()+m2.getGrad());
                pol.add(rez);
            }
        }

        for(int i=0;i<pol.size()-1;i++) {
            Monom m = pol.get(i);
            int s = m.getCoef();
            for (int j = i + 1; j < pol.size(); j++) {
                if (m.getGrad().equals(pol.get(j).getGrad())) {
                    s += pol.get(j).getCoef();
                    pol.remove(j);
                }
            }
            m.setCoef(s);
        }
        return pol;
    }

    public List<Monom> derivare(){
        List<Monom> pol=new ArrayList<>();
        for(int i=0;i<p1.size();i++){
            Monom m=p1.get(i);
            Monom rez=new Monom(m.getCoef()*m.getGrad(),m.getGrad()-1);
            pol.add(rez);
        }
        return pol;
    }

    public List<Monom> integrare() {
        List<Monom> pol = new ArrayList<>();
        for (int i = 0; i < p1.size(); i++) {
            Monom m = p1.get(i);
            Monom rez = new Monom(m.getCoef()/(m.getGrad()+1),m.getGrad()+1);
            pol.add(rez);
        }
        return pol;
    }

    @Override
    public String toString(){
        String result="";
        Comparator<Monom> comparator=(monom1, monom2)->{return monom2.getGrad()-monom1.getGrad();};
        this.p1.sort(comparator);
        Monom ultim=p1.remove(this.p1.size()-1);
        for(Monom monom:this.p1){
            if (monom.getCoef()==0)
                result+="";
            else if (monom.getCoef()==1)
                result+="X^"+monom.getGrad()+"+";
            else if(monom.getCoef()==-1)
                result+="-X^"+monom.getGrad()+"+";
            else if(monom.getCoef()>1)
                result+=monom.getCoef()+"X^"+monom.getGrad()+"+";
            else
                result+="-"+monom.getCoef()+"X^"+monom.getGrad();
        }
        if(ultim.getGrad()==0){
            result+=ultim.getCoef();
        }
        else if(ultim.getCoef()==1){
            result += "X^" + ultim.getGrad();
        }
        else if(ultim.getCoef()==-1)
            result+="-X^"+ultim.getGrad();
        else if(ultim.getCoef()>1)
            result+=ultim.getCoef()+"X^"+ultim.getGrad();
        else
            result+="-"+ultim.getCoef()+"X^"+ultim.getGrad();
        return result;
    }
}

